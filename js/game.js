// Ball(x, y, size)
  // direction: 
// Brick(index, level, size, type)
  // type: 'normal', 'brick'
let color = []
for (let i = 0; i < 6; i++){
  for (let j = 0; j < 6; j++){
    color.push('rgb(' + Math.floor(255 - 42.5 * i) + ',' + Math.floor(255 - 42.5 * j) + ',0)')
  }
}

const Game = function (canvas, ctx, rectSize) {
  if (!canvas || !ctx || !rectSize) {
    alert('不知道出了什么鬼错误~~还是别玩了，好好学习')
    return
  }
  this.canvas = canvas
  ctx.globalCompositeOperation  = 'hard-light' // 设置图像合成模式为覆盖
  this.ctx = ctx
  this.rectSize = rectSize
  this.ballSize = rectSize / 2
  this.state = {
    ballNum: 1, // 球的数量
    level: 1, // 当前游戏等级
    brickList: [[new Brick(1, 1, rectSize, 'normal')]], // 砖块二维数组
    ballList: [new Ball(rectSize * 3.5, rectSize * 10.5, this.ballSize), new Ball(rectSize * 3.5, rectSize * 10.5, this.ballSize), new Ball(rectSize * 3.5, rectSize * 10.5, this.ballSize), new Ball(rectSize * 3.5, rectSize * 10.5, this.ballSize), new Ball(rectSize * 3.5, rectSize * 10.5, this.ballSize)], // 球队列
    animationSpeed: 10, // 动画速度
    isAnimating: false, // 是否正在进行动画
    dropPosition: null
  }
  util.addEvent(canvas, 'click', ({clientX, clientY}) => {
    canvasRect = canvas.getBoundingClientRect()
    let x = clientX - canvasRect.left - this.ballSize / 2
    let y = clientY - canvasRect.top - this.ballSize / 2
    this.launchBall(x, y)
  })
}

Game.prototype.drawBrick = function() { // 绘制方块
  let ctx = this.ctx
  let brickList = this.state.brickList
  for (let i = brickList.length - 1; i >= 0; i--) { // 第i + 1行
    let brickListRow = brickList[i]
    for (let j = brickListRow.length - 1; j >= 0; j--) { // 第j + 1个
      let brick = brickListRow[j] // 那个砖块
      switch(brick.type) { // 根据砖块类型来决定绘制方法
        case 'normal': {
          // 方块
          ctx.fillStyle = color[parseInt(brick.level / 50)] // 取砖块颜色, 每50是一个等级
          ctx.fillRect((brick.index - 1) * brick.size + 1, i * brick.size + 1, brick.size - 1, brick.size - 1) // 绘制方块
          // 方块内的数字
          ctx.fillStyle = 'black'
          ctx.font = "30px serif"
          let level = brick.level.toString()
          ctx.fillText(level, (brick.index - 1) * brick.size + brick.size * ((.3 - (level.length - 1) * .2) > 0 ? (.3 - (level.length - 1) * .2) : 0), i * brick.size + brick.size * .8, brick.size)
          break
        }
        default: {
          console.log('Can\'t match the type of the brick, and\nbrick.type: ' + brick.type)
        }
      }
    }
  }
}

Game.prototype.drawBall = function() {
  let ctx = this.ctx
  let ballList = this.state.ballList
  if (!this.state.isAnimating) {
    let ball = ballList[0]
    ctx.drawImage(ballImg, ball.x, ball.y, ball.size, ball.size)
  } else {
    for (let i = ballList.length - 1; i >= 0; i--) {
      let ball = ballList[i]
      ctx.drawImage(ballImg, ball.x, ball.y, ball.size, ball.size)
    }
  }
}

Game.prototype.launchBall = function(x, y) { // 传入点击的区域
  if (this.state.isAnimating) {
    return false
  } else {
    console.log('launchBall')
    // 只需要取到第一个球的位置
    let ball = this.state.ballList[0]
    // 球的位置
    let ballX = ball.x
    let ballY = ball.y
    // 偏移量的总和
    let total = (x - ballX) > 0 ? (x - ballX) - (y - ballY) : -((x - ballX) + (y - ballY))
    // 偏移相加为1
    let offset = {
      x: 5 * (x - ballX) / total,
      y: 5 * (y - ballY) / total
    }
    this.animate(offset) // 传入初始偏移量
  }
}

Game.prototype.animate = function({x, y}) { // 球的初始偏移量，进行动画
  this.state.isAnimating = true
  let {
    animationSpeed, // 每一帧动画之间的间隔
    ballList, // 球的列表
    ballNum, // 球的数目
    brickList, // 砖块二维数组
    level // 当前游戏等级
  } = this.state
  let frameNum = 0 // 当前动画帧
  let tag = true // 标志动画队列是否完成
  let len = ballList.length
  let lastBall = ballList[len - 1] // 最后一个球
  let launchIndex = 0
  console.log('x:', x) // 球的发射偏移量
  console.log('y:', y) // 球的发射偏移量
  for (let i = 0; i < len; i++) {
    ballList[i].direction = {
      x: x,
      y: y
    }
  }
  let intervalHandle = setInterval(() => {
    for (let i = 0; i < len; i++) {
      if (i <= (frameNum++) / 50) {
        let ball = ballList[i]
        // 下一帧的x和y
        let nextX = ball.x + ball.direction.x
        let nextY = ball.y + ball.direction.y
        if (this.dropPosition && this.dropPosition.y - nextY < 0.0000000001) {
          nextX = this.dropPosition.x
          nextY = this.dropPosition.y
        } else { // 判断下一帧是否发生碰撞
          if (nextX <= 0 || nextX > this.rectSize * 7.5) {
            console.log('ball hit left wall OR hit right wall')
            ball.direction.x = -ball.direction.x
          } else if (nextY <= 0) {
            console.log('ball hit top wall')
            ball.direction.y = -ball.direction.y
          } else if (nextY >= this.rectSize * 10.5) {
            console.log('ball hit bottom wall')
            if (!this.dropPosition) {
              this.dropPosition = {
                x: nextX,
                y: this.rectSize * 10.5
              }
              nextY = this.rectSize * 10.5
            } else {
              ball.direction = {
                x: (this.dropPosition.x - nextX) / 10,
                y: 0
              }
              nextY = this.rectSize * 10.5
            }
          }          
        }

        // 下一帧的位置
        ball.x = nextX
        ball.y = nextY
      } else {
        break
      }
    }
    console.log(lastBall)
    console.log(this.dropPosition)
    if (this.dropPosition && this.dropPosition.x === lastBall.x && this.dropPosition.y === lastBall.y) {
      console.log('animate stop')
      this.state.isAnimating = false
      this.dropPosition = null
      clearInterval(intervalHandle)
    }
    this.draw()
  }, animationSpeed)
}

Game.prototype.draw = function () {
  this.ctx.clearRect(0, 0, this.rectSize * 8, this.rectSize * 11)
  this.drawBall()
  this.drawBrick()
}